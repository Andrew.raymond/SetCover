from time import time
from functools import wraps
import os
import errno
import signal

class Profiler():

	@classmethod
	def timer(cls):
		def middle(func):
			def inner(*args, **kwargs):
				start = time()
				rtn = func(*args, **kwargs)
				end = time()

				runtime = time() - start
				print("%s took %02.6f seconds." % (func.__name__, runtime))

				return rtn
			return inner
		return middle
