from itertools import combinations
from profiler import Profiler

#Global Vars
universe = []
subsets  = []

'''
HELPER FUNCTIONS Include:
	1. readInformation 	- void
	2. isSubset 		- boolean
	3. coversTheSEt 	- boolean
	4. preSearchPruning - void 
	5. MinSetCover 		- void
'''

#This is for the initial reading of the information for the text file provided.
def readInformation(fp):
	#fp = input("Enter the filepath to the txt file: ")
	with open(fp, "r") as f:					#opens the filepointer as f
			global universe						#this is U
			global subsets 						#this is S

			univSize 	= f.readline()[:-1] 	#gets rid of /n at the end of the lines
			numSubsets 	= f.readline()[:-1] 	#same thing ^
			
			for i in range(1, int(univSize)+1):	
				universe.append(i)				#Populate the universal set

			lines = f.readlines()				#Places all of the next lines into "lines" 
			for i in range(len(lines)):		
				lines[i] = lines[i].split()		#Tokenizes based on spaces
				lines[i] = [int(l) for l in lines[i]]
				subsets.append(lines[i]) 		#Populates the subset set

#Prunes out some things before we loop
def preSearchPruning(S, U):
 	#Loop through the subset list S
	print("Initial pruning...")
	for i in range(len(S)):
		if coversTheSet(U, S) == False:	
			S.pop(i)				#Prunes out the subsets that do not have any elements in common main set
			if (i == len(S)): break #This was the last element! So break the for-loop

def coversTheSet(mainSet, testSet):
	resultUnion = list(set().union(*testSet))	#Unions all of the sets together
	if set(mainSet).issubset(set(resultUnion)):	return True #Can check if the union is a subset of the target
	else:	return False

#The main recursive function
#U = the main set, S = the subsets, notUncovered is what we have left to find for the min set cover
def MinSetCover(S, U):	
	minSetCover = [None]
	level = 1								#Start on level 1
	while(minSetCover == [None]):			#Continue until answer is found
		for i in combinations(S, level):	#loop through the unique combinations of subsets with size "level"
			if coversTheSet(U, i):	
				print("There is a set cover at level " + str(level) + " of " + str(i))
				minSetCover = i
				break
			else: pass	
		level+=1							#increment the level

readInformation("D:/Users/Andy/Desktop/School/Senior Year/CSE373/SetCover/SetCoverTxts/s-k-30-50.txt")
preSearchPruning(subsets, universe)
MinSetCover(subsets, universe)