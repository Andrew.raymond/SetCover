#Global Variables
global univSize
global numSubsets
global currentMin	#What we will use to hold the best minimum cover set we have found, so far



'''
HELPER FUNCTIONS Include:
	1. readInformation 	- void
	2. isSubset 		- boolean
	3. inCommon 		- list
	4. removeInCommon 	- list
	5. preSearchPruning - void
'''
#This is for the initial reading of the information for the text file provided.
def readInformation(fp):
	#fp = input("Enter the filepath to the txt file: ")
	try:
		with open(fp, "r") as f:
			univSize 	= f.readline()[:-1] #gets rid of /n
			numSubsets 	= f.readline()[:-1] 
			print("The universal set size is " + univSize) 
			print("The number of subsets is "  + numSubsets)
	except:
		print("No such filepath " + fp) 

#Determines if a set is a subset of a main list
def isSubset(subset, mainList):
	
	return	all(i in mainList for i in subset)

#Returns a list of the common elements
def inCommon(subset, mainList):
	common = []
	for i in mainList:
		if i in subset:
			common.append(i)
	return common

#Returns the mainList without the elements from subset
def removeInCommon(subset, mainList):
	ret = []
	for i in mainList:
		if i not in subset:
			ret.append(i)
	return ret

#Prunes out some stuff
def preSearchPruning(S, U):
 	#Loop through the subset list U
	print("Initial pruning...")
	for i in range(len(S)):
		if isSubset(S[i], U) == False:	
			print ("PRUNE: " + str(S[i]) + " is not subset of the main set.")
			S.pop(i)					#Prunes out the subsets that do not have any elements in common with the main set
			if (i == len(S)): break 	#This was the last element! So break the for loop
	print ("\n")


#The main recursive function
#U = the main set, S = the subsets, notUncovered is what we have left to find for the min set cover
workingMin  = [] 						#What we will use to construct the current minimum cover set
def MinSetCover(S, U, notUncovered):	#Always begin with notUncovered as S (for recursion)
	#Loop through the subset list S
	print(range(len(S)))
	print(str(S))
	for i in range(len(S)):
		print(S[i])
		#Work through all of the subsets linearly
		workingMin.append(S[i])								#Add the current to the workingMin list
		S.pop(i)											#Remove the current from the Subset list
		notUncovered = removeInCommon(S[i], notUncovered)	#Remove the current from the list of things that are not covered
		
	'''Prune any workingMin sets that are larger than our current candidate for the min set cover.
	try:
		if len(currentMin) > len(workingMin):
			print("The currentMin is bigger than the workingMin, so we can just stop here.")
			#Backtrack here
	except:
		pass
		#Current min is not defined yet, so doesn't matter
	'''

	#At this point we have an element chosen for the largest uncovered, so we can add that to the workingMin set
	print ("S: " + str(S))
	print ("Working Minimum Set: " + str(workingMin))
	print ("Not uncovered yet: "   + str(notUncovered))

	#Recursively loop until all elements are covered
	if (len(notUncovered) is not 0): 
		print("\nRECURSING!")
		MinSetCover(S, U, notUncovered)	


	#Otherwise, we have a minimum set cover candidate (it may not be right)
	else:
		currentMin = workingMin
		print("We have a current min now... but it may not be right yet: " + str(currentMin))
		#BACKTRACK HERE


'''
Test Case:
	Correct min set cover should be [[1, 3, 5], [2, 4, 6]]
'''
U = [1, 2, 3, 4, 5, 6]
S = [
[1, 2], 
[1, 3, 5], 
[1, 2, 3, 4], 
[2, 4, 6], 
[1, 5], 
[2, 6], 
[8, 9]
]

preSearchPruning(S, U)
MinSetCover(S, U, U)